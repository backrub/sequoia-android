package com.jumper.hangouts.common;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * @author Meeth D Jain
 */
public class BusProvider {

    private static final Bus REST_BUS = new Bus(ThreadEnforcer.ANY);
    private static final Bus UI_BUS = new Bus();
    private BusProvider() {}

    public static Bus getRestBus() {
        return REST_BUS;
    }

    public static Bus getUiBus() {
        return UI_BUS;
    }
}
