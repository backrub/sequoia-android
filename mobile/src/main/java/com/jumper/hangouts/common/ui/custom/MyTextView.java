package com.jumper.hangouts.common.ui.custom;

import android.content.Context;
import android.widget.TextView;

/**
 *
 * @author Meeth D Jain
 */
public class MyTextView extends TextView {

    public MyTextView(Context context) {
        super(context, null);
    }
}
