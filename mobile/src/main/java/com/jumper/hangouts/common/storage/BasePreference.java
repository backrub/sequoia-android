package com.jumper.hangouts.common.storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Meeth D Jain
 */
public class BasePreference {

    private SharedPreferences sharedPreferences;

    public BasePreference(Context context, PreferenceType preferenceType) {
        sharedPreferences = context.getSharedPreferences(preferenceType.filename, Context.MODE_PRIVATE);
    }

    public synchronized boolean getBoolean(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public synchronized float getFloat(String key, float defaultValue) {
        return sharedPreferences.getFloat(key, defaultValue);
    }

    public synchronized int getInt(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public synchronized String getString(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public synchronized void setBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public synchronized void setFloat(String key, float value) {
        sharedPreferences.edit().putFloat(key, value).apply();
    }

    public synchronized void setInt(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public synchronized void setString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public synchronized void clear() {
        sharedPreferences.edit().clear().apply();
    }

    public enum PreferenceType {

        CONFIGURATION ("configuration"),
        SESSION ("session");

        private String filename;

        PreferenceType(String filename) {
            this.filename = filename;
        }
    }
}
