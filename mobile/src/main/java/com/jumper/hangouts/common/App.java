package com.jumper.hangouts.common;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * @author Meeth D Jain
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        AppManager.getInstance().setApplicationContext(this);
        Fabric.with(this, new Crashlytics());
    }
}
