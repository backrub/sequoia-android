package com.jumper.hangouts.common;

import android.app.Application;
import android.content.Context;


/**
 * @author Meeth D Jain
 */
public class AppManager {

    private static AppManager ourInstance = new AppManager();

    private Context applicationContext;

    private AppManager() {
    }

    public static AppManager getInstance() {
        return ourInstance;
    }

    public Context getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(Application applicationContext) {
        this.applicationContext = applicationContext;
    }
}
