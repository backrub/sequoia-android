package com.jumper.hangouts.common.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 *
 * @author Meeth D Jain
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
