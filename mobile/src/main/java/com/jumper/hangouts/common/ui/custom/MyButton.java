package com.jumper.hangouts.common.ui.custom;

import android.content.Context;
import android.widget.Button;

/**
 *
 * @author Meeth D Jain
 */
public class MyButton extends Button {

    public MyButton(Context context) {
        super(context, null);
    }
}
