package com.jumper.hangouts.plans.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jumper.hangouts.events.model.Event;

import java.util.List;

/**
 * @author Meeth D Jain
 */
public class PlanAdapter extends BaseAdapter {

    private List<Event> mEvents;

    public PlanAdapter(List<Event> events) {
        this.mEvents = events;
    }

    @Override
    public int getCount() {
        return mEvents.size();
    }

    @Override
    public Event getItem(int position) {
        return mEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    static class ViewHolder {

        public ViewHolder(View view) {

        }
    }
}
