package com.jumper.hangouts.components.services;

import android.app.IntentService;
import android.content.Intent;

/**
 * @author Meeth D Jain
 */
public class GCMIntentService extends IntentService {

    public GCMIntentService() {
        super(GCMIntentService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }
}
