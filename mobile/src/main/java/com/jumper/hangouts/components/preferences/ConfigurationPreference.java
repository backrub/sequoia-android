package com.jumper.hangouts.components.preferences;

import com.jumper.hangouts.common.AppManager;
import com.jumper.hangouts.common.storage.BasePreference;

/**
 *
 * @author Meeth D Jain
 */
public class ConfigurationPreference extends BasePreference {

    private static ConfigurationPreference ourInstance = new ConfigurationPreference();

    private ConfigurationPreference() {
        super(AppManager.getInstance().getApplicationContext(), PreferenceType.CONFIGURATION);
    }

    public static ConfigurationPreference getInstance() {
        return ourInstance;
    }

    @Override
    public synchronized void clear() {
        //Overridden to prevent clear.
    }
}
