package com.jumper.hangouts.components.preferences;

import com.jumper.hangouts.common.AppManager;
import com.jumper.hangouts.common.storage.BasePreference;

/**
 * @author Meeth D Jain
 */
public class SessionPreference extends BasePreference {

    private static SessionPreference ourInstance = new SessionPreference();

    private SessionPreference() {
        super(AppManager.getInstance().getApplicationContext(), PreferenceType.SESSION);
    }

    public static SessionPreference getInstance() {
        return ourInstance;
    }
}
