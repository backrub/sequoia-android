package com.jumper.hangouts.components.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.jumper.hangouts.BuildConfig;

/**
 * @author Meeth D Jain
 */
public class CoreService extends Service {

    public static final String ACTION_START_CORE = BuildConfig.APPLICATION_ID + ".START_CORE";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();

            if (ACTION_START_CORE.equals(action)) {

            }

        } else {
            stopSelf();
        }

        return START_REDELIVER_INTENT;
    }
}
