package com.jumper.hangouts.rest;

import com.jumper.hangouts.rest.services.RestService;

/**
 * @author Meeth D Jain
 */
public class ApiManager extends RestAdapter {

    private static ApiManager ourInstance = new ApiManager();
    private RestService mRestService;

    private ApiManager() {
    }

    public static ApiManager getInstance() {
        return ourInstance;
    }
}
