package com.jumper.hangouts.rest;

/**
 * @author Meeth D Jain
 */
public interface RestConstants {

    String HEADER_VERSION_CODE = "version_code";
    String HEADER_VERSION_NAME = "version_name";
}
