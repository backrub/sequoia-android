package com.jumper.hangouts.events.model;

import com.google.android.gms.location.places.Place;

/**
 * @author Meeth D Jain
 */
public class Event {

    private String creator;
    private String eventName;
    private String timestamp;

    private Place place;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
