package com.jumper.hangouts.events.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jumper.hangouts.common.ui.BaseFragment;

/**
 * @author Meeth D Jain
 */
public class PlanFragment extends BaseFragment {

    public static String getMyTag() {
        return PlanFragment.class.getSimpleName();
    }

    public static PlanFragment newInstance(Bundle bundle) {
        PlanFragment fragment = new PlanFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
