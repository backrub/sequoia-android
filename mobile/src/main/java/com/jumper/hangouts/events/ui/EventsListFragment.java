package com.jumper.hangouts.events.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jumper.hangouts.R;
import com.jumper.hangouts.common.ui.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Meeth D Jain
 */
public class EventsListFragment extends BaseFragment {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainFragment.
     */
    public static EventsListFragment newInstance() {
        return new EventsListFragment();
    }

    public static String getMyTag() {
        return PlanAddFragment.class.getSimpleName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        setupToolbar(mToolbar, R.string.title_fragment_main, true, true, android.R.drawable.btn_star);
        return rootView;
    }
}
