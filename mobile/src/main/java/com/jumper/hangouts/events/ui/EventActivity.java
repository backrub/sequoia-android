package com.jumper.hangouts.events.ui;

import android.os.Bundle;

import com.jumper.hangouts.R;
import com.jumper.hangouts.common.ui.BaseActivity;

/**
 * @author Meeth D Jain
 */
public class EventActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, PlanAddFragment.newInstance(), PlanAddFragment.getMyTag())
                    .commit();
        }
    }

    public interface EventCallback {
    }
}
