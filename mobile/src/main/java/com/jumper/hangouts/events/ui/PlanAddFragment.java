package com.jumper.hangouts.events.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.jumper.hangouts.R;
import com.jumper.hangouts.common.ui.BaseFragment;
import com.jumper.hangouts.common.ui.DatePickerFragment;
import com.jumper.hangouts.common.ui.TimePickerFragment;
import com.jumper.hangouts.common.util.DateUtil;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Meeth D Jain
 */
public class PlanAddFragment extends BaseFragment implements DatePickerFragment.OnDateSelectedListener, TimePickerFragment.OnTimeSelectedListener {

    private static final int PLACE_PICKER_REQUEST = 1;

    @Bind(R.id.btn_event_date_time)
    Button mEventDateTimeBtn;
    @Bind(R.id.btn_event_location)
    Button mEventLocationBtn;
    @Bind(R.id.img_view_place)
    ImageView mPlaceImgView;
    @Bind(R.id.txt_view_event_place_address)
    TextView mPlaceAddressTxtView;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    private Calendar mCalendar;
    private GoogleApiClient mGoogleApiClient;
    private Place mPlace;
    private PlacePicker.IntentBuilder mPlacePickerBuilder;
    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
            = new ResultCallback<PlacePhotoResult>() {
        @Override
        public void onResult(PlacePhotoResult placePhotoResult) {
            if (!placePhotoResult.getStatus().isSuccess()) {
                return;
            }
            Bitmap bitmap = placePhotoResult.getBitmap();
            if (bitmap != null) {
                mPlaceImgView.setImageBitmap(placePhotoResult.getBitmap());
                mPlaceImgView.setVisibility(View.VISIBLE);
            } else {
                mPlaceImgView.setVisibility(View.GONE);
            }
        }
    };

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventAddFragment.
     */
    public static PlanAddFragment newInstance() {
        return new PlanAddFragment();
    }

    public static String getMyTag() {
        return PlanAddFragment.class.getSimpleName();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCalendar = Calendar.getInstance();
        mCalendar.setTime(DateUtil.getDateRoundedToMin(30));
        mPlacePickerBuilder = new PlacePicker.IntentBuilder();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_event_add, container, false);
        ButterKnife.bind(this, rootView);
        setupToolbar(mToolbar, R.string.title_fragment_event_add, true, true, R.drawable.ic_toolbar_close);

        mEventDateTimeBtn.setText(DateUtil.getFormattedDateTime(mCalendar.getTime()));
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.event_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home : {
                getActivity().onBackPressed();
                return true;
            }
            case R.id.action_event_create : {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @SuppressWarnings("unused")
    @OnClick({R.id.btn_event_date_time, R.id.btn_event_location})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_event_date_time: {
                Bundle bundle = DatePickerFragment.getBundle(mCalendar.get(Calendar.YEAR),
                        mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH));
                DatePickerFragment.newInstance(bundle, this).show(getFragmentManager(), DatePickerFragment.getMyTag());
                break;
            }
            case R.id.btn_event_location: {
                launchPlacePicker();
                break;
            }
        }
    }

    @Override
    public void onDateSelected(int year, int month, int day) {
        mCalendar.set(year, month, day);
        Bundle bundle = TimePickerFragment.getBundle(mCalendar.get(Calendar.HOUR_OF_DAY),
                mCalendar.get(Calendar.MINUTE),
                true);
        TimePickerFragment.newInstance(bundle, this).show(getFragmentManager(), TimePickerFragment.getMyTag());
    }

    @Override
    public void onTimeSelected(int hourOfDay, int minute) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);
        //Set the selected date time value.
        mEventDateTimeBtn.setText(DateUtil.getFormattedDateTime(mCalendar.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                setPlaceDetails(place);
            }
        }
    }

    private void launchPlacePicker() {
        try {
            startActivityForResult(mPlacePickerBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void setPlaceDetails(Place place) {
        if (place != null) {
            mPlace = place;
            mEventLocationBtn.setText(mPlace.getName());
            mPlaceAddressTxtView.setText(mPlace.getAddress());
            mPlaceAddressTxtView.setVisibility(View.VISIBLE);
            placePhotosAsync();
        }
    }

    private void placePhotosAsync() {
        Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, mPlace.getId())
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {

                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }

                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the first bitmap in an ImageView in the size of the view
                            photoMetadataBuffer.get(0)
                                    .getPhoto(mGoogleApiClient)
                                    .setResultCallback(mDisplayPhotoResultCallback);
                        }
                        photoMetadataBuffer.release();
                    }
                });
    }
}
